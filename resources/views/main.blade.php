<!DOCTYPE html>
<html>
  <head>


    {{--Every child page should inject page title through section name title--}}
    @include('partials.MainPartials._head')
    <link href="{{ asset('css/footer.css') }}" rel="stylesheet">
    @yield('OuterInclude')
    <style type="text/css">
      #ContentOfBody{
        bottom: 0;
        left: 0;
        position: relative;
        right: 0;
        top: 0;
        min-height: 100vh;
      }
      #ContentOfooter{
        bottom: 0;
        left: 0;
        position: relative;
        right: 0;
        top: 5em;
        /* min-height: 100vh; */

        
    }

    .navbar-nav li a:hover {
          color: #1abc9c !important;
          background: transparent !important;
      }

    body{
      background: url("picture/a.jpg") fixed center;
    }
    </style>

      <style>
          .News
          {
              background-color: rgb(243, 239, 239);
          }
          .News h2
          {
              padding-bottom:30px;
              text-align: center;
          }
          .crop
          {
              object-fit: cover;
              height:100%;
          }
          .details
          {
              font-size:16px;
              color:rgb(94, 89, 89);
          }
          .latest
          {
              background-color:seashell;
              padding:25px 20px;
          }
          .latest h4
          {
              color:#002868;
              font-size: 25px;
          }
          .latest h3
          {
              padding:10px 0;
          }
          .latest p
          {
              padding:15px 0;
          }
          .news-img-container
          {
              overflow:hidden;
              height:300px;
          }
          .news-img-container img
          {
              width:100%;
          }
      </style>


  </head>

  <body>
     @include('partials.MainPartials._navigation')
      <div id="ContentOfBody" class="container-fluid">
         @yield('ContentOfBody')
      </div>

     <div id="ContentOfooter">
     <footer class="footer">
      <div class="container">
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
              <h4 style="text-align: center">Online Appointment System For Hospital</h4>
              <h5 style="text-align: center">Bringing Clinics, Doctors & Patients all together in one platform. </h5>
              <h5 style="text-align: center">It provides a fastest way to book an appointment with the desired practioner and helps to avoid long queue of patient waiting in the clinic.</h5>
            </div>
            <div class="col-sm-3"></div>
          </div>
          <hr>

          <div class="row text-center"> © 2019. Developed by <a style="text-decoration: none" href="#">Kritika Pandey</a></div>
      </div>
    </footer>
     </div>
     <script src="https://khalti.com/static/khalti-checkout.js"></script>
     <script>
         var config = {
             // replace the publicKey with yours
             "publicKey": "test_public_key_cc519165be9a4c4aa5ef5b045976dbb2",
             "productIdentity": "1234567890",
             "productName": "Dragon",
             "productUrl": "http://gameofthrones.wikia.com/wiki/Dragons",
             "eventHandler": {
                 onSuccess (payload) {
                     // hit merchant api for initiating verfication
                     $.ajax({
                         url: "{{url('/payments/verification')}}",
                         type: 'GET',
                         data: {
                             amount: payload.amount,
                             trans_token: payload.token
                         },
                         success: function (res) {
                             console.log("transaction success");
                         },
                         error: function (error) {
                             console.log("transaction failed");
                         }
                     })
                     console.log(payload);
                 },
                 onError (error) {
                     console.log(error);
                 },
                 onClose () {
                     console.log('widget is closing');
                 }
             }
         };
         var checkout = new KhaltiCheckout(config);
         var btn = document.getElementById("payment-button");
         btn.onclick = function () {
             checkout.show({amount: 1000});
         }
     </script>


  </body>

</html>
