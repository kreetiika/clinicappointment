@extends('main')

{{-- Including  required CSS/JS/Other --}}

@section('title')
    Home
@endsection

@section('OuterInclude')

    {{-- <script src="{{ asset('js/profile_update.js') }}"></script> --}}
@endsection
@section('ContentOfBody')
<div class="container">
  <br>
  <br>
    <h1 class="text-center" style="color: #1f648b; font-weight: bold;"> <b> WELCOME TO DOCTOR APPOINTMENT SYSTEM</b></h1>
    <hr>
    <br>
    
    <h1 class="text-center" style="font-size: 40px; font-weight:bold; color: #FFA500" >
      <marquee>Find and Book a Doctor Appointment right now</marquee>
    </h1>

    <section class="News padding-top padding-bottom">
        <div class="container">
            <h2 class="heading">Doctor Details</h2>
            <div class="row">
                <div class="col-lg-4 ">
                    <div class="latest">
                        <h4>Dermatologist</h4>
                        <div class="news-img-container">
                            <img class="crop" src="images/doctor2.jpg" alt="image">
                        </div>
                        <p class="details">Vestibulum libero nisl,porto
                            vel,scelerisque eget,malisuada at,neque,Vestibulum libero nisl,porto
                            vel,scelerisque eget,malisuada at,neque.Vestibulum libero nisl,porto
                            vel.</p>
                        <h5 class="details">July,25 2020</h5>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="latest">
                        <h4>Gynecologist</h4>
                        <div class="news-img-container">
                            <img class="crop" src="images/doctor1.jpg" alt="image">
                        </div>
                        <p class="details">Vestibulum libero nisl,porto
                            vel,scelerisque eget,malisuada at,neque,Vestibulum libero nisl,porto
                            vel,scelerisque eget,malisuada at,neque.Vestibulum libero nisl,porto
                            vel.</p>
                        <h5 class="details">July,30 2020</h5>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="latest">
                        <h4>Cardiologist</h4>
                        <div class="news-img-container">
                            <img class="crop" src="images/doctor3.jpg" alt="image">
                        </div>
                        <p class="details">Vestibulum libero nisl,porto
                            vel,scelerisque eget,malisuada at,neque,Vestibulum libero nisl,porto
                            vel,scelerisque eget,malisuada at,neque.Vestibulum libero nisl,porto
                            vel.</p>
                        <h5 class="details">August,25 2020</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

@endsection

